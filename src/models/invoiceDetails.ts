import mongoose, { Schema, Document } from 'mongoose';


export interface IinvoiceDetails extends Document {
    invoiceId: mongoose.Types.ObjectId;
    franchiseProductId: mongoose.Types.ObjectId;
    quantity: Number;
    price: Number;
    type: String

}

const invoiceDetails: Schema = new Schema({
    invoiceId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'invoice',
        required: true
    },

    franchiseProductId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'franchise-product',
        required: true
    },

    date: { type: Date },
    billType: { type: String },

    paymentMethod: { type: String },
    quantity: { type: Number },
    unitPrice: { type: Number },
    type: { type: String }


}, { timestamps: true });

// Export the model and return interface
export default mongoose.model<IinvoiceDetails>('invoice-details', invoiceDetails);