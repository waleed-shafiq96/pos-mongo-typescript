import mongoose, { Schema, Document } from 'mongoose';


export interface Isupplier extends Document {
    storeId: mongoose.Schema.Types.ObjectId;
    franchiseId: mongoose.Schema.Types.ObjectId;
    firstName: string;
    lastName : string
    companyName : string;
    address: string;
    productType: string;
    contactNo: string;
    email: string;
    isDeleted: Boolean;
}

const supplier: Schema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    franchiseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'franchise',
        required: true
    },
    firstName: { type: String, required: true },
    lastName: { type: String },
    companyName: { type: String },
    address: { type: String },
    accountNo: { type: String },
    productType: { type: String },
    contactNo: { type: String },
    email: { type: String },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });

// Export the model and return interface
export default mongoose.model<Isupplier>('supplier', supplier);