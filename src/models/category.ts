import mongoose, { Schema, Document } from 'mongoose';


export interface Icategory extends Document {
    storeId: mongoose.Schema.Types.ObjectId;
    name: string;
    subCategories: any;
}

const category: Schema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'store'
    },
    name: { type: String, required: true, unique: true },
    subCategories: [{ type: String }],
}, { timestamps: true });

// Export the model and return interface
export default mongoose.model<Icategory>('category', category);