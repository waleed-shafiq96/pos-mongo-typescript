import mongoose, { Schema, Document } from 'mongoose';


export interface IfranchiseProduct extends Document {
    storeId: mongoose.Schema.Types.ObjectId;
    frachiseId: mongoose.Schema.Types.ObjectId;
    productId: mongoose.Schema.Types.ObjectId;
    supplierId: mongoose.Schema.Types.ObjectId;
    invoiceId: mongoose.Schema.Types.ObjectId;
    unitPrice: Number;
    quantiy: Number;


}

const franchiseProduct: Schema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    franchiseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'franchise',
        required: true
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'products'
    },
    supplierId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'supplier'
    },
    invoiceId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'invoice'
    },
    unitPrice: { type: Number },
    quantity: { type: Number },

}, { timestamps: true });

// Export the model and return interface
export default mongoose.model<IfranchiseProduct>('franchise-product', franchiseProduct);