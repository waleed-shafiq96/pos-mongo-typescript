import mongoose, { Schema, Document } from 'mongoose';


export interface Ifranchise extends Document {
    storeId: mongoose.Schema.Types.ObjectId;
    address: string;
    isDeleted: Boolean;
}

const franchise: Schema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    name: {
        type: String,
        required: true,
        default: 'main franchise'
    },
    address: {
        type: String
    },
    phone: { type: String },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });

// Export the model and return interface
export default mongoose.model<Ifranchise>('franchise', franchise);