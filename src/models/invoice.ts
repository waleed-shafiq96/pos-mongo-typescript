import mongoose, { Schema, Document } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


export interface Iinvoice extends Document {
    storeId: mongoose.Types.ObjectId;
    franchiseId: mongoose.Types.ObjectId;
}

const invoice: Schema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    franchiseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'franchsie',
        required: true
    },
    customer: { type: String, default: null },
    supplierId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'supplier',
        default: null
    },
    netTotal: { type: Number },
    paid: { type: Number },
    type: { type: String },



}, { timestamps: true });

autoIncrement.initialize(mongoose.connection)
autoIncrement.plugin(invoice, {
    model: 'invoice',
    field: 'invoiceNo',
    startAt: 1,
    incrementBy: 1
});

// Export the model and return interface
export default mongoose.model<Iinvoice>('invoice', invoice);