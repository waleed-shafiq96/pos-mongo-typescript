import { Application } from 'express';
import { createFranchise, deleteFranchise, findFranchises, findFranchise, editFranchise } from '../controllers/franchise';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';
import { createFranchiseValidation, idValidation } from '../middlewares/validations'


const franchise = (app: Application) => {
    const prefix = '/franchise';

    app.post(prefix + '/createfranchise', verify, softDelete, createFranchise);
    app.post(prefix + '/deletefranchise', verify, softDelete, idValidation, deleteFranchise);
    app.get(prefix + '/findallfranchises', verify, softDelete, findFranchises);
    app.post(prefix + '/findfranchise', verify, softDelete, idValidation, findFranchise);
    app.post(prefix + '/editfranchise', verify, softDelete, editFranchise);




}

export default franchise;