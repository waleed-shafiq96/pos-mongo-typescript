import storeUser from './storeUser';
import auth from './auth';
import store from './store';
import franchise from './franchise';
import franchiseProduct from './franchiseProduct';
import products from './products';
import invoice from './invoice';
import supplier from './supplier';
import category from './category';
import report from './report';

import { Request, Response, Application } from 'express';


const index = (app: Application) => {
    app.get('/', (req: Request, res: Response) => {
        return res.json({ message: 'hello' });
    })
}




const allRoutes = (app: Application) => {
    return {
        root: index(app),
        storeUser: storeUser(app),
        auth: auth(app),
        store: store(app),
        franchise: franchise(app),
        franchiseProduct: franchiseProduct(app),
        products: products(app),
        invoice: invoice(app),
        supplier: supplier(app),
        category: category(app),
        report: report(app)
    }
}
export default allRoutes