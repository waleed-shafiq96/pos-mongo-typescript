import { login } from '../controllers/auth'
import { Application } from 'express';
import {loginValidation} from '../middlewares/validations';

const auth = (app: Application) => {
    const prefix = '/auth';


    app.post(prefix + '/login', loginValidation, login);




}

export default auth;