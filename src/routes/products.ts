import { Application } from 'express';
import { viewProducts, addGenericProduct, viewProductsByCategory, editGenericProduct } from '../controllers/products';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';
import { genericProductValidation, idValidation } from '../middlewares/validations';


const products = (app: Application) => {

    const prefix = '/products';

    app.get(prefix + '/allproducts', verify, softDelete, viewProducts);
    app.post(prefix + '/addproduct', verify, softDelete, addGenericProduct);
    app.post(prefix + '/categoryproducts', verify, softDelete, idValidation, viewProductsByCategory);
    app.post(prefix + '/editproduct', verify, softDelete, editGenericProduct);

}

export default products;