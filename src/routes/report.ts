import { Application } from 'express';
import { stockReport, saleReport } from '../controllers/report'
import { signupValidation, idValidation } from '../middlewares/validations';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';



const report = (app: Application) => {
    const prefix = '/report';

    app.get(prefix + '/stock', verify, softDelete, stockReport);
    app.get(prefix + '/sale', saleReport);

}

export default report;