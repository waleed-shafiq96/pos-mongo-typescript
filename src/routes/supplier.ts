import { Application } from 'express';
import { addSupplier, storeSuppliers, franchiseSuppliers, deleteSupplier, editSupllier } from '../controllers/supplier';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';



const supplier = (app: Application) => {
    const prefix = '/supplier';

    app.get(prefix + '/viewsuppliers', verify, softDelete, storeSuppliers);

    app.post(prefix + '/viewfranchisesuppliers', verify, softDelete, franchiseSuppliers);


    app.post(prefix + '/addfranchisesupplier', verify, softDelete, addSupplier);

    app.post(prefix + '/deletesupplier', verify, softDelete, deleteSupplier);

    app.post(prefix + '/editsupplier', verify, softDelete, editSupllier);



}

export default supplier;