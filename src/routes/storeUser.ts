import { Application } from 'express';
import { addStoreUser, deleteStoreUser, findStoreUsers, findFranchiseStoreUsers, findRoleStoreUsers ,editStoreUser} from '../controllers/storeUser';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';
import { idValidation } from '../middlewares/validations';



const storeUser = (app: Application) => {
    const prefix = '/storeuser';

    app.post(prefix + '/addstoreuser', verify, softDelete, addStoreUser);
    app.post(prefix + '/deletestoreuser', verify, softDelete, deleteStoreUser);
    app.post(prefix + '/findallstoreusers', verify, softDelete, findStoreUsers);
    app.post(prefix + '/findfranchisestoreusers', verify, softDelete, findFranchiseStoreUsers);
    app.post(prefix + '/findrolestoreusers', verify, softDelete, findRoleStoreUsers);
    app.post(prefix + '/edituser', verify, softDelete , editStoreUser);


}

export default storeUser;