import { Application } from 'express';
import { allCategories, createCategory, addSubCategory, editCategory } from '../controllers/category';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';
import { createCategoryValidation, subCategoryValidation } from '../middlewares/validations';


const category = (app: Application) => {

    const prefix = '/category';

    app.get(prefix + '/allcategories', verify, softDelete, allCategories);
    app.post(prefix + '/addcategory', verify, softDelete, createCategory);
    app.post(prefix + '/addsubcategory', verify, softDelete, addSubCategory);
    app.post(prefix + '/editcategory', verify, softDelete, editCategory);



}

export default category;