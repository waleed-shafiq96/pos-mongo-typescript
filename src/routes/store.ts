import { Application } from 'express';
import { createStore, findStore, allStores, deleteStore, allStoresAndFranchises, settings, getSetting } from '../controllers/store';
import { signupValidation, idValidation } from '../middlewares/validations';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';



const store = (app: Application) => {
    const prefix = '/store';

    app.post(prefix + '/createstore', signupValidation, createStore);
    app.post(prefix + '/deletestore', idValidation, deleteStore);
    app.post(prefix + '/findstore', findStore);
    app.post(prefix + '/allstores', allStores);
    app.get(prefix + '/allstoresandfranchises', allStoresAndFranchises);
    app.post(prefix + '/editstore', verify, softDelete, settings);
    app.get(prefix + '/getstore', verify, softDelete, getSetting);




}

export default store;