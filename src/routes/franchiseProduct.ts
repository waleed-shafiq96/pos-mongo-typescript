import { Application } from 'express';
import { addProduct, findFranchiseProductById, findStoreProductById, findStoreAllProducts, findFranchiseAllproducts, deleteStoreProduct, deleteFranchiseProduct } from '../controllers/franchiseProduct';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';
import { franchiseProductValidation, idValidation } from '../middlewares/validations';


const franchiseProduct = (app: Application) => {

    const prefix = '/franchiseproduct';

    app.post(prefix + '/addproduct', verify, softDelete, addProduct);
    app.post(prefix + '/findfranchiseproduct', verify, softDelete, idValidation, findFranchiseProductById);
    app.post(prefix + '/findproduct', verify, softDelete, idValidation, findStoreProductById);
    app.post(prefix + '/findfranchiseproducts', verify, softDelete, findFranchiseAllproducts);
    app.get(prefix + '/findstoreproducts', verify, softDelete, findStoreAllProducts);
    app.post(prefix + '/deletefranchiseproduct', verify, softDelete, idValidation, deleteFranchiseProduct);
    app.post(prefix + '/deletestoreproduct', verify, softDelete, idValidation,deleteStoreProduct);









}

export default franchiseProduct;