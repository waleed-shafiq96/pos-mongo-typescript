import { Application } from 'express';
import { createInvoice, findInvoice, allPurchaseInvoices, allSaleInvoices, editPurchaseInvoice } from '../controllers/invoice';
import verify from '../middlewares/auth/verify.middleware';
import softDelete from '../middlewares/auth/softDelete';


const invoice = (app: Application) => {
    const prefix = '/invoice';


    app.post(prefix + '/createinvoice', verify, softDelete, createInvoice);
    app.post(prefix + '/findinvoice', verify, softDelete, findInvoice);
    app.post(prefix + '/allsaleinvoices', verify, softDelete, allSaleInvoices);
    app.post(prefix + '/allpurchaseinvoices', verify, softDelete, allPurchaseInvoices);
    app.post(prefix + '/editinvoice', verify, softDelete, editPurchaseInvoice);


}

export default invoice;