import { Request, Response } from 'express'
import bcrypt from 'bcryptjs';
import StoreUser from '../models/storeUser';
import store from '../models/store';
import franchise from '../models/franchise';
import Category from '../models/category';



/*
*** Api for viewing all categories***
*/

const allCategories = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        const category: any = await Category.find({ storeId: storeUser.storeId.id }, 'name subCategories');

        if (!category[0]) {
            return res.status(404).json({ msg: 'No categories found' });
        }

        res.status(200).json({ msg: 'categories found ', categories: category })
    } catch (error) {

    }
};

/*
*** Api for creating a category for products of a store***
*/
const createCategory = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }
        const category: any = await Category.updateOne({
            storeId: storeUser.storeId.id,
            name: req.body.categoryName
        }, {}, { upsert: true });

        if (category.upserted) {
            return res.status(200).json({ msg: 'category created' });
        }

        res.status(200).json({ msg: 'category updated' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for adding  subcategory to a category***
*/
const addSubCategory = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }

        const existingCategory: any = await Category.findOne({
            storeId: storeUser.storeId.id,
            _id: req.body.categoryId,
            subCategories: { $in: [req.body.subCategory] }
        }, '_id');

        if (existingCategory) {
            return res.status(400).json({ msg: "sub category already exist" });
        }

        const subcategory: any = await Category.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body.categoryId
        }, {
            $push: { subCategories: req.body.subCategory }
        });

        if (subcategory.nModified === 0) {
            return res.status(400).json({ msg: 'sub category not added' })
        }

        res.status(200).json({ msg: 'sub category added' });
    } catch (error) {
        console.log('error:::::', error)
        res.status(500).json({ msg: error.message })
    }
};


/*
*** Api for editing  a category***
*/
const editCategory = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }
        const category: any = await Category.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body._id
        }, {
            name: req.body.name
        });

        if (category.nModified == 0) {
            return res.status(400).json({ msg: 'category not edited' });
        }

        res.status(201).json({ msg: 'category updated' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


export {
    createCategory,
    allCategories,
    addSubCategory,
    editCategory

}