import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import storeUser from '../models/storeUser';
import store from '../models/store';
import franchise from '../models/franchise';
import roles from '../models/roles';


/*
*** Super Admin Api for Store Creation ***
*/
const createStore = async (req: Request, res: Response) => {
    try {
        if (!req.body.storeName || !req.body.address || !req.body.email || !req.body.password) {
            return res.status(403).json({ msg: 'Fields Missing' });
        }
        const storeExists = await store.findOne({ storeName: req.body.storeName });
        if (storeExists) {
            return res.status(409).json({ msg: 'store Already Exist with this name' })
        }

        const storeName: any = req.body.storeName.split(' ').join('');
        const newStore: any = await store.create({
            storeName: req.body.storeName,
            domain: storeName + '.rs.com'
        });


        const newFranchise: any = await franchise.create({
            storeId: newStore.id,
            address: req.body.address
        })

        const hashedPass: any = await bcrypt.hash(req.body.password, 12);

        const role: any = await roles.findOne({ roleName: 'store admin' });
        if (!role) {
            return res.status(404).json({ msg: 'No role Found ' })
        }

        const newStoreUser: any = await storeUser.create({
            storeId: newStore.id,
            role: role.id,
            email: req.body.email,
            password: hashedPass
        });

        res.status(200).json({ msg: 'Store Created', store: newStore, franchise: newFranchise, storeUser: newStoreUser });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for finding a Store***
*/
const findStore = async (req: Request, res: Response) => {
    try {
        const currentStore = await store.findOne({ storeName: req.body.storeName });
        if (!currentStore) {
            return res.status(404).json({ msg: 'No store found of this name' })
        }
        res.status(200).json({ msg: 'store found', store: currentStore });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
};


/*
*** Api for soft deleting a Store***
*/

const deleteStore = async (req: Request, res: Response) => {
    try {

        const deletedStore: any = await store.updateOne({
            _id: req.body.id,
            isDeleted: false
        }, { isDeleted: true });

        if (deletedStore.nModified === 0) {
            return res.status(400).json({ msg: 'No store Deleted' });
        }

        res.status(200).json({ msg: 'Store soft Deleted' });
    } catch (error) {
        res.status(500).json({ msg: 'Internal sever error' })
    }
};

/*
*** Api for finding all Stores***
*/
const allStores = async (req: Request, res: Response) => {
    try {
        const allStores: any = await store.find();
        if (!allStores[0]) {
            return res.status(404).json({ msg: 'No stores' });
        }
        res.status(200).json({ msg: 'Stores found', store: allStores });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

/*
*** Api for finding all Stores and thier franchises***
*/

const allStoresAndFranchises = async (req: Request, res: Response) => {
    try {
        const allStores: any = await franchise.find().populate('storeId');
        if (!allStores[0]) {
            return res.status(404).json({ msg: 'No stores and franchises' });
        }
        res.status(200).json({ msg: 'Stores and franchises found', store: allStores });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
};

/*
*** Api for posting settings of a Store***
*/
const settings = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not Authorized' });
        }

        const currentStore = await store.updateOne({
            _id: storeUser.storeId.id
        }, {
            storeName: req.body.storeName,
            storeAddress: req.body.storeAddress,
            ownerName: req.body.ownerName,
            phone: req.body.phone,
            slogan: req.body.slogan,
            email: req.body.email
        });

        if (currentStore.nModified == 0) {
            return res.status(404).json({ msg: 'No store edited of this name' })
        }
        res.status(200).json({ msg: 'store edited' });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
};



/*
*** Api for getting settings of a Store***
*/
const getSetting = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not Authorized' });
        }

        const currentStore = await store.findOne({
            _id: storeUser.storeId.id
        });

        if (!currentStore) {
            return res.status(404).json({ msg: 'No store found' })
        }
        res.status(200).json({ msg: 'store found', store: currentStore });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
};


export {
    createStore,
    findStore,
    allStores,
    deleteStore,
    allStoresAndFranchises,
    settings,
    getSetting
}