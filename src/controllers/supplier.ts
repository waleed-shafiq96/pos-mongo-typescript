import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import storeUser from '../models/storeUser';
import store from '../models/store';
import franchise from '../models/franchise';
import Supplier from '../models/supplier';


/*
*** Api for viewing all suppliers of a store ***
*/
const storeSuppliers = async (req: Request, res: Response) => {
    try {
        const storeUser: any = req.body.user;

        const suppliers: any = await Supplier.find({
            storeId: storeUser.storeId.id,
            isDeleted: false
        }).populate('franchiseId', 'name');

        if (!suppliers[0]) {
            return res.status(404).json({ msg: 'No suppliers Found' });
        }

        res.status(200).json({ msg: 'Suppliers found', suppliers: suppliers });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

/*
*** Api for viewing all suppliers of a franchise ***
*/
const franchiseSuppliers = async (req: Request, res: Response) => {
    try {
        const storeUser: any = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const suppliers: any = await Supplier.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            isDeleted: false
        }, '_id firstName companyName address contactNo franchiseId');

        if (!suppliers[0]) {
            return res.status(200).json({ msg: 'No franchise suppliers Found' });
        }

        res.status(200).json({ msg: 'franchise Suppliers found', suppliers: suppliers });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}


/*
*** Api for adding a supplier of a franchise ***
*/
const addSupplier = async (req: Request, res: Response) => {
    try {

        const storeUser: any = req.body.user;

        if (storeUser.role.roleName === 'store amdin' && !req.body.franchiseId) {
            return res.status(400).json({ msg: 'Must provide Franchise Id' });
        }

        const newSupplier: any = await Supplier.updateOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            companyName: req.body.companyName,
            contactNo: req.body.phone,
        }, {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            accountNo: req.body.accountNo,
            address: req.body.address,
            email: req.body.email,
            isDeleted: false
        }, { upsert: true });

        if (newSupplier.upserted) {
            return res.status(200).json({ msg: 'Supplier created' });
        }

        res.status(201).json({ msg: 'supplier updated' })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

/*
*** Api for deleting a supplier of a franchise ***
*/
const deleteSupplier = async (req: Request, res: Response) => {
    try {
        const storeUser: any = req.body.user;

        if (storeUser.role.roleName === 'store amdin' && !req.body.franchiseId) {
            return res.status(400).json({ msg: 'Must provide Franchise Id' });
        }

        const newSupplier: any = await Supplier.updateOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            _id: req.body.supplierId,
            isDeleted: false
        }, {
            isDeleted: true
        })
        if (newSupplier.nModified === 0) {
            return res.status(400).json({ msg: 'Supplier not deleted' });
        }

        res.status(201).json({ msg: 'supplier soft deleted' })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

/*
*** Api for editing a supplier ***
*/
const editSupllier = async (req: Request, res: Response) => {
    try {
        const storeUser: any = req.body.user;
        console.log('body:::', req.body);

        if (storeUser.role.roleName === 'store amdin' && !req.body.franchiseId) {
            return res.status(400).json({ msg: 'Must provide Franchise Id' });
        }

        const newSupplier: any = await Supplier.updateOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName == 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            _id: req.body._id
        }, {
            companyName: req.body.companyName,
            contactNo: req.body.contactNo,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            accountNo: req.body.accountNo,
            address: req.body.address,
            email: req.body.email,
        });

        if (newSupplier.nModified == 0) {
            return res.status(400).json({ msg: 'Supplier Not Edited' });
        }

        res.status(201).json({ msg: 'supplier edited' })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

export {
    storeSuppliers,
    franchiseSuppliers,
    addSupplier,
    deleteSupplier,
    editSupllier

}