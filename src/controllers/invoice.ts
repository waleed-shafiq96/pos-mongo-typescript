import { Request, Response } from 'express'

import StoreUser from '../models/storeUser';
import FranchiseProducts from '../models/franchise-product';
import InvoiceDetails from '../models/invoiceDetails';
import Invoice from '../models/invoice';
import Supplier from '../models/supplier';
import FranchiseProduct from '../models/franchise-product';




/*
*** Api for creating a invoice***
*/
const createInvoice = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        if (!req.body.products && !Array.isArray(req.body.products)) {
            return res.status(400).json({ msg: 'NO prodcuts or products is not an array' })
        }

        const invoice: any = await Invoice.create({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            customer: req.body.customerType,
            netTotal: req.body.netTotal,
            paid: req.body.paid,
            type: 'sale'
        });

        if (!invoice) {
            return res.status(400).json({ msg: 'sale invoice not created' });
        }
        let total = 0;
        const mappedProducts: any = req.body.products.map((p: any) => {
            total = total + p.price * p.quantity;
            return {
                invoiceId: invoice.id,
                franchiseProductId: p.productId,
                date: req.body.date,
                billType: req.body.billType,
                paymentMethod: req.body.paymentMethod,
                unitPrice: p.unitPrice,
                quantity: p.quantity,
                type: 'sale'
            }
        });

        const invoiceDetails: any = await InvoiceDetails.insertMany(mappedProducts);

        res.status(200).json({
            msg: 'Invoice Created', totalPrice: total, GST: '17%',
            grandTotal: (17 / 100 * total) + total,
            invoice: invoiceDetails
        });

        mappedProducts.map(async (p: any) => {
            await FranchiseProducts.updateOne({
                _id: p.franchiseProductId
            }, {
                $inc: { quantity: - p.quantity }
            });
        });

    } catch (error) {
        console.log('error:::::', error)
        res.status(500).json({ msg: error.message })
    }
};

/*
*** Api for finding an invoice***
*/
const findInvoice = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        if (!req.body.invoiceId) {
            return res.status(400).json({ msg: 'must provide invoice id' })
        }

        var invoice: any = await InvoiceDetails.find({
            invoiceId: req.body.invoiceId
        }).populate({ path: 'invoiceId', populate: { path: 'supplierId', select: 'firstName' } }).populate({ path: 'franchiseProductId', select: '_id productId', populate: { path: 'productId', select: 'name _id' } });

        if (!invoice[0]) {
            return res.status(404).json({ msg: 'No invoice found' });
        }

        const suppliers: any = await Supplier.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            isDeleted: false
        }, '_id firstName companyName address contactNo franchiseId');

        var outData = {
            suppliers: {
                _id: invoice[0].invoiceId.supplierId.id,
                firstName: invoice[0].invoiceId.supplierId.firstName
            },
            invoiceNo: invoice[0].invoiceId.invoiceNo,
            netTotal: invoice[0].invoiceId.netTotal,
            paid: invoice[0].invoiceId.paid,
            date: invoice[0].date,
            paymentMethod: invoice[0].paymentMethod,
            billType: invoice[0].billType,
        }
        var obj: any = invoice.map((p: any) => {
            var obj1 = { ...p._doc };
            delete obj1.invoiceId
            obj1.productName = obj1.franchiseProductId.productId.name;
            obj1.productId = obj1.franchiseProductId.productId.id;
            delete obj1.franchiseProductId
            return obj1
        });

        res.status(200).json({ msg: 'Invoice found', outDetails: outData, invoice: obj, suppliers: suppliers });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for returning a purchased product***
*/

const returnProducts = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (!storeUser) {
            return res.status(401).json({ msg: 'No user found' });
        }
        if (storeUser.role.roleName !== 'franchise admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }

        if (!req.body.invoiceId || !req.body.products) {
            return res.status(400).json({ msg: 'must provide all fields' })
        }

        if (Array.isArray(req.body.products)) {
            return res.status(400).json({ msg: 'prodcuts is not an array' })
        }

        const invoice: any = await Invoice.findOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.franchiseId.id,
            _id: req.body.invoiceId
        });

        if (!invoice) {
            return res.status(404).json({ msg: 'No invoice found' });
        }

        res.status(200).json({ msg: 'Invoice found and items returned' });

        req.body.products.map((p: any) => {

        });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

/*
*** Api for viewing all sale invoices of a fracnhise ***
*/
const allSaleInvoices = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const invoice: any = await Invoice.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            type: 'sale'
        }, 'createdAt invoiceNo netTotal paid customer franchiseId');

        if (!invoice[0]) {
            return res.status(404).json({ msg: 'No invoice found' });
        }
        res.status(200).json({ msg: 'Invoice found', invoices: invoice })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for viewing all purchase invoices of a fracnhise ***
*/
const allPurchaseInvoices = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;
        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const invoice: any = await Invoice.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            type: 'purchase'
        }, 'createdAt invoiceNo netTotal paid supplierId franchiseId').populate('supplierId');

        if (!invoice[0]) {
            return res.status(404).json({ msg: 'No invoice found' });
        }
        res.status(200).json({ msg: 'Invoice found', invoices: invoice })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

/*
*** Api for editing a invocie of a fracnhise ***
*/
const editPurchaseInvoice = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        if (!req.body.invoiceId) {
            return res.status(400).json({ msg: 'must provide invoice id' })
        }

        const deletedInvoice: any = await Invoice.deleteOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            _id: req.body.invoiceId
        });


        const deletedDetails: any = await InvoiceDetails.deleteMany({ invoiceId: req.body.invoiceId });

        const invoice: any = await Invoice.create({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            supplierId: req.body.supplierId ? req.body.supplierId : null,
            netTotal: req.body.netTotal,
            paid: req.body.paid,
            type: 'purchase'
        });

        if (!invoice) {
            return res.status(400).json({ msg: 'purchase invoice not created' });
        }

        await FranchiseProduct.deleteMany({ invoiceId: req.body.invoiceId });

        const franchiseProduct: any = req.body.products.map(async (p: any) => {
            return await FranchiseProduct.create({
                storeId: storeUser.storeId.id,
                franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
                supplierId: req.body.supplierId,
                productId: p.productId,
                invoiceId: invoice.id,
                unitPrice: p.unitPrice,
                quantity: p.quantity
            });
        });

        Promise.all(franchiseProduct).then(async (...args) => {
            const newProducts: any = args[0].map((p: any) => {
                return {
                    invoiceId: invoice.id,
                    franchiseProductId: p.id,
                    date: req.body.date,
                    billType: req.body.billType,
                    paymentMethod: req.body.paymentMethod,
                    unitPrice: p.unitPrice,
                    quantity: p.quantity
                }
            });


            const invoiceDetails: any = await InvoiceDetails.insertMany(newProducts);

            res.status(200).json({ msg: 'Products edited' });

        }).catch(error => {
            return res.status(500).json({ msg: error.message });
        });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


export {
    createInvoice,
    findInvoice,
    returnProducts,
    allSaleInvoices,
    allPurchaseInvoices,
    editPurchaseInvoice
}   