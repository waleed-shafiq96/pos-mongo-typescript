import { Request, Response } from 'express'
import fs from 'fs';
import path from 'path';

import Products from '../models/products';
import Category from '../models/category';

import { upload } from '../utitly/aws';

/*
*** Api for viewing generic products***
*/
const viewProducts = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (storeUser.role.roleName !== 'store admin' && storeUser.role.roleName !== 'franchise admin') {
            return res.status(403).json({ msg: 'Not authorized' })
        }

        const products: any = await Products.find({ storeId: storeUser.storeId.id }).populate('category', 'name -_id');
        if (!products[0]) {
            return res.status(404).json({ msg: 'No products' });
        }

        res.status(200).json({ msg: 'products found', products: products });
    } catch (error) {
        return res.status(500).json({ msg: error.message })
    }
};


/*
*** Api for adding generic products***
*/
const addGenericProduct = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (!req.body.productName || !req.body.categoryId || !req.body.productCode) {
            return res.status(400).json({ msg: 'Fields missing' })
        }
        // decode base64 and upload to directory
        if (req.body.imageField && req.body.imageField.filename) {
            var imageBuffer: any = new Buffer(req.body.imageField.base64.split(',')[1], 'base64');
            var fileName = Date.now() + req.body.imageField.filename;

            var image: any = await upload(fileName, imageBuffer, req.body.imageField.filetype, req);
            if (image.hasOwnProperty('Location')) {
                req.body.imageField = image.Location
            } else {
                return res.status(400).json({ status: false, msg: 'Error uploading image' });
            }
        }

        const newProduct: any = await Products.updateOne({
            storeId: storeUser.storeId.id,
            name: req.body.productName,
            productCode: req.body.productCode,
            category: req.body.categoryId
        }, {
            costPrice: req.body.costPrice,
            openingStock: req.body.openingStock || 1,
            minimumQuantity: req.body.minimumQuantity,
            maximumQuantity: req.body.maximumQuantity,
            minimumRetailPrice: req.body.minimumRetailPrice,
            productTax: req.body.productTax,
            unit: req.body.unit,
            status: req.body.status,
            description: req.body.description,
            imageField: req.body.imageField || ''
        }, { upsert: true })

        if (newProduct.upserted) {
            return res.status(200).json({ msg: 'Product added' });
        }

        res.status(201).json({ msg: 'prodcut updated' });
    } catch (error) {
        res.status(500).json({ msg: error.message })
    }
}

/*
*** Api for viewing generic products by category***
*/
const viewProductsByCategory = async (req: Request, res: Response) => {
    try {

        const products: any = await Products.find({
            category: req.body.id
        });

        if (!products[0]) {
            return res.status(404).json({ msg: 'no products found of this category' });
        }

        res.status(200).json({ msg: 'prodcuts found ', products: products })
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

/*
*** Api for viewing generic products by category***
*/
const editGenericProduct = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (req.body.imageField && req.body.imageField.filename) {
            var imageBuffer: any = new Buffer(req.body.imageField.base64.split(',')[1], 'base64');
            var fileName: any = Date.now() + req.body.imageField.filename;

            var image: any = await upload(fileName, imageBuffer, req.body.imageField.filetype, req);
            if (image.hasOwnProperty('Location')) {
                req.body.imageField = image.Location
            } else {
                return res.status(400).json({ status: false, msg: 'Error uploading image' });
            }
        }

        const category: any = await Category.findOne({ name: req.body.category });
        if (!category) {
            return res.status(404).json({ status: false, msg: 'No category Found' });
        }

        const newProduct: any = await Products.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body._id
        }, {
            name: req.body.name,
            category: category.id,
            productCode: req.body.productCode,
            costPrice: req.body.costPrice,
            openingStock: req.body.openingStock || 1,
            minimumQuantity: req.body.minimumQuantity,
            maximumQuantity: req.body.maximumQuantity,
            minimumRetailPrice: req.body.minimumRetailPrice,
            productTax: req.body.productTax,
            unit: req.body.unit,
            status: req.body.status,
            description: req.body.description,
            imageField: req.body.imageField || ''
        })

        if (newProduct.nModified == 0) {
            return res.status(200).json({ msg: 'Product not edited' });
        }

        res.status(201).json({ msg: 'prodcut edited' });
    } catch (error) {
        res.status(500).json({ msg: error.message })
    }
};


export {
    viewProducts,
    addGenericProduct,
    viewProductsByCategory,
    editGenericProduct
}