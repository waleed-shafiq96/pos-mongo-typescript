import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import StoreUser from '../models/storeUser';
import roles from '../models/roles';
import Franchise from '../models/franchise';




/*
*** Api for creating a store User***
*/
const addStoreUser = async (req: Request, res: Response) => {

    try {
        const storeUser = req.body.user;
        if (!req.body.role || !req.body.email || !req.body.password || !req.body.franchiseId) {
            return res.status(403).json({ msg: 'Fields Missing' });
        }

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }

        const existingUser: any = await StoreUser.findOne({
            storeId: storeUser.storeId.id,
            email: req.body.email
        });
        if (existingUser) {
            return res.status(409).json({ msg: 'User already exist with this email' })
        }

        const role = await roles.findOne({ roleName: req.body.role });
        if (!role) {
            return res.status(400).json({ msg: 'No role Found' });
        }

        const hashedPass: any = await bcrypt.hash(req.body.password, 12);

        const existingfranchise: any = await Franchise.findOne({
            storeId: storeUser.storeId.id,
            _id: req.body.franchiseId
        }, '_id isDeleted');

        if (!existingfranchise || existingfranchise.isDeleted) {
            return res.status(400).json({ msg: 'no franchise found' });
        }

        const newStoreUser = await StoreUser.create({
            storeId: storeUser.storeId.id,
            franchiseId: existingfranchise.id,
            role: role.id,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hashedPass,
            address: req.body.address,
            phone: req.body.phone

        });

        if (!newStoreUser) {
            return res.status(404).json({ msg: 'store User not created' });
        }

        res.status(200).json({ msg: 'Store user created', storeUser: newStoreUser });
    } catch (error) {
        console.log(error)
        res.status(500).json({ msg: error.message });

    }
}


/*
*** Api for deleting a store User***
*/
const deleteStoreUser = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (!req.body.storeUserId) {
            return res.status(403).json({ msg: 'Fields Missing' });
        }

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }

        const newStoreUser: any = await StoreUser.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body.storeUserId,
            isDeleted: false
        }, {
            isDeleted: true
        });

        if (newStoreUser.nModified === 0) {
            return res.status(400).json({ msg: 'store user not deleted' })
        }

        res.status(200).json({ msg: 'Store user deleted' });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });

    }
}

/*
*** Api for finding all store Users***
*/

const findStoreUsers = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }

        const storeUsers: any = await StoreUser.find({
            storeId: storeUser.storeId.id,
            isDeleted: false
        }).populate('role', '_id roleName');

        if (!storeUsers[0]) {
            return res.status(404).json({ msg: 'No store Users found' })
        }

        res.status(200).json({ msg: 'Store users found', storeUsers: storeUsers });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

/*
*** Api for finding store Users of a franchise***
*/
const findFranchiseStoreUsers = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const storeUsers: any = await StoreUser.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            isDeleted: false
        });

        if (!storeUsers[0]) {
            return res.status(404).json({ msg: 'No store Users found' })
        }

        res.status(200).json({ msg: 'franchise Store users found', storeUsers: storeUsers });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });

    }
}


/*
*** Api for finding store Users by role***
*/
const findRoleStoreUsers = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (!req.body.roleName) {
            return res.status(400).json({ msg: 'must provide roleName' })
        }
        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }
        const role: any = await roles.findOne({ roleName: req.body.roleName });
        if (!role) {
            return res.status(404).json({ msg: 'No role found' });
        }


        const storeUsers: any = await StoreUser.find({
            storeId: storeUser.storeId.id,
            role: role.id,
            isDeleted: false
        });

        if (!storeUsers[0]) {
            return res.status(404).json({ msg: 'No store Users found of this role' })
        }

        res.status(200).json({ msg: 'users found', storeUsers: storeUsers });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });

    }
}

/*
*** Api for editing a store User***
*/
const editStoreUser = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' });
        }


        const editedUser: any = await StoreUser.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body.id,
            isDeleted: false
        }, {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            address: req.body.address,
            phone: req.body.phone
        });

        if (editedUser.nModified == 0) {
            return res.status(404).json({ msg: 'No store User edited of this id' })
        }

        res.status(200).json({ msg: 'Store user edited', storeUsers: editedUser });
    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

export {
    addStoreUser,
    deleteStoreUser,
    findStoreUsers,
    findFranchiseStoreUsers,
    findRoleStoreUsers,
    editStoreUser
}