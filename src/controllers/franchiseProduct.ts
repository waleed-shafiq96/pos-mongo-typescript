import { Request, Response } from 'express'
import franchise from '../models/franchise';
import FranchiseProduct from '../models/franchise-product';
import Invoice from '../models/invoice';
import InvoiceDetails from '../models/invoiceDetails';

/*
*** Api for adding products to a franchise***
*/
const addProduct = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;
        console.log('body::::', req.body);

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const existingFranchise: any = await franchise.findOne({
            storeId: storeUser.storeId.id,
            _id: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            isDeleted: false
        });

        if (!existingFranchise) {
            return res.status(404).json({ msg: 'No frachise found to add product to' });
        }

        const invoice: any = await Invoice.create({
            storeId: storeUser.storeId.id,
            franchiseId: existingFranchise.id,
            supplierId: req.body.supplierId ? req.body.supplierId : null,
            netTotal: req.body.netTotal,
            paid: req.body.paid,
            type: 'purchase'
        });

        if (!invoice) {
            return res.status(400).json({ msg: 'purchase invoice not created' });
        }

        const franchiseProduct: any = req.body.products.map(async (p: any) => {
            return await FranchiseProduct.create({
                storeId: storeUser.storeId.id,
                franchiseId: existingFranchise.id,
                supplierId: req.body.supplierId ? req.body.supplierId : null,
                productId: p.productId,
                invoiceId: invoice.id,
                unitPrice: p.unitPrice,
                quantity: p.quantity

            });
        });

        Promise.all(franchiseProduct).then(async (...args) => {
            const newProducts: any = args[0].map((p: any) => {
                return {
                    invoiceId: invoice.id,
                    franchiseProductId: p.id,
                    date: req.body.date,
                    billType: req.body.billType,
                    paymentMethod: req.body.paymentMethod,
                    unitPrice: p.unitPrice,
                    quantity: p.quantity,
                    type: 'purchase'
                }
            });


            const invoiceDetails: any = await InvoiceDetails.insertMany(newProducts);

            res.status(200).json({ msg: 'Products added' });

        }).catch(error => {
            return res.status(500).json({ msg: error.message });
        });

    } catch (error) {
        console.log('error:::::', error)
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for finding a store product by product id**
*/
const findStoreProductById = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        const product: any = await FranchiseProduct.findOne({
            storeId: storeUser.storeId.id,
            productId: req.body.id,
            quantity: { $gt: 0 }
        });

        if (!product) {
            return res.status(404).json({ msg: 'No product found' });
        }

        res.status(200).json({ msg: 'product found', product: product })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}


/*
*** Api for finding a franchise product by product id**
*/
const findFranchiseProductById = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const product: any = await FranchiseProduct.findOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            productId: req.body.id,
            quantity: { $gt: 0 }
        });

        if (!product) {
            return res.status(404).json({ msg: 'No product found' });
        }

        res.status(200).json({ msg: 'product found', product: product })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}


/*
*** Api for finding all products of a franchise***
*/
const findFranchiseAllproducts = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const product: any = await FranchiseProduct.find({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            quantity: { $gt: 0 }
        }).populate('productId');

        if (!product[0]) {
            return res.status(404).json({ msg: 'No product found' });
        }
        res.status(200).json({ msg: 'product found', product: product })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for finding all products of a Store***
*/
const findStoreAllProducts = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' })
        }

        const product: any = await FranchiseProduct.find({
            storeId: storeUser.storeId.id,
            quantity: { $gt: 0 }
        });

        if (!product[0]) {
            return res.status(404).json({ msg: 'No product found' });
        }

        res.status(200).json({ msg: 'product found', product: product })

    } catch (error) {
        console.log('error:::::', error)
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for deleting a product of a franchise***
*/
const deleteFranchiseProduct = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName === 'store admin' && !req.body.franchiseId) {
            return res.status(403).json({ msg: 'must provide franchiseId ' })
        }

        const product: any = await FranchiseProduct.updateOne({
            storeId: storeUser.storeId.id,
            franchiseId: storeUser.role.roleName === 'store admin' ? req.body.franchiseId : storeUser.franchiseId.id,
            _id: req.body.id,
            quantity: { $gt: 0 }
        }, {
            quantity: 0
        });

        if (product.nModified === 0) {
            return res.status(400).json({ msg: 'product not deleted' });
        }

        res.status(201).json({ msg: 'product deleted' });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};


/*
*** Api for deleting a product of a store***
*/
const deleteStoreProduct = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;


        if (storeUser.role.roleName !== 'store admin') {
            return res.status(403).json({ msg: 'Not authorized' })
        }

        const product: any = await FranchiseProduct.updateMany({
            storeId: storeUser.storeId.id,
            productId: req.body.id,
            quantity: { $gt: 0 }
        }, {
            quantity: 0
        });

        if (product.nModified === 0) {
            return res.status(400).json({ msg: 'product not deleted' });
        }

        res.status(201).json({ msg: 'product deleted' });

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}


/*
*** Api for updating a product of a franchise***
*/
const updateFranchiseProduct = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (!req.body.franchiseProductId) {
            return res.status(400).json({ msg: 'Must provide all fields' });
        }
        if (!storeUser) {
            return res.status(404).json({ msg: 'No store User found' });
        }

        if (storeUser.role.roleName !== 'store admin' && storeUser.role.roleName !== 'franchise admin') {
            return res.status(403).json({ msg: 'Not authorized' })
        }

        // const newFranchiseProduct: any = await FranchiseProduct.updateOne({
        //     storeId: storeUser.storeId.id,
        //     _id: req.body.franchiseProductId
        // }, {
        //     quantity ? (quantity : req.body.quantity ) : 
        // })

    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

export {
    addProduct,
    findStoreProductById,
    deleteFranchiseProduct,
    deleteStoreProduct,
    findFranchiseProductById,
    findFranchiseAllproducts,
    findStoreAllProducts,
    updateFranchiseProduct
}