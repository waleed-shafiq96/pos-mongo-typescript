import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import storeUser from '../models/storeUser';
import store from '../models/store';
import franchise from '../models/franchise';
import { adminJwt, jwt } from '../utitly/jwt';



const login = async (req: Request, res: Response) => {
    try {
        if (!req.body.storeId || !req.body.email || !req.body.password) {
            return res.status(403).json({ msg: 'fields Missing' })
        }
        const user: any = await storeUser.findOne({
            storeId: req.body.storeId,
            email: req.body.email
        }).populate('storeId').populate('role').populate('franchiseId');

        if (!user) {
            return res.status(404).json({ msg: 'No user found' });
        }

        const isEqual: any = await bcrypt.compare(req.body.password, user.password);
        if (!isEqual) {
            return res.status(401).json({ msg: 'Wrong password' });
        }

        if (user.isDeleted || user.storeId.isDeleted) {
            return res.status(401).json({ msg: 'User or store is soft deleted' });
        }

        if (user.role.roleName !== 'store admin') {
            if (user.franchiseId.isDeleted) {
                return res.status(401).json({ msg: 'franchise is soft deleted' });
            }
            const token: any = jwt(user);
            return res.status(200).json({ msg: 'login successfull', token: token , user : user });
        }

        const token: any = adminJwt(user);
        return res.status(200).json({ msg: ' Admin login successfull', token: token, user: user });
    } catch (error) {
        console.log('eror::::', error);
        res.status(404).json({ msg: error.message });
    }
};


export {
    login,
}