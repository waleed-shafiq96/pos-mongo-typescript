import { Request, Response } from 'express'
import invoiceDetails from '../models/invoiceDetails';
import FranchiseProduct from '../models/franchise-product'
import { Db } from 'mongodb';

/*
*** Api for stock report***
*/

const stockReport = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(400).json({ msg: ' Not Authorized or Missing franchise Id' })
        }

    
        const report = await FranchiseProduct.aggregate([
            {
                $lookup: {
                    "from": "invoice-details",
                    "let": {
                        productId: "$_id"
                    },
                    "pipeline": [
                        {
                            $match: {
                                $expr: {
                                    $and: [{
                                        $eq: [
                                            "$$productId",
                                            "$franchiseProductId"
                                        ]
                                    },
                                    {
                                        $eq: ["$type", "sale"]
                                    }
                                    ]

                                }
                            }
                        }
                    ],
                    as: "invoice"
                }
            },
            {
                $unwind: {
                    path: "$invoice"
                }
            },
            // {
            //     $match: {
            //         "invoice.type": "purchase"
            //     }
            // },
            // {
            //     $unset: "invoice"
            // }
        ]);

        if (!report[0]) {
            return res.status(404).json({ msg: ' No stock in this franchise' });
        }
        await FranchiseProduct.populate(report, { path: 'productId', select: '-_id name productCode costPrice' });

        return res.status(200).json({ report });
    } catch (error) {
        return res.status(500).json({ msg: error.message });
    };
};

const saleReport = async (req: Request, res: Response) => {
    try {
        const response = await FranchiseProduct.aggregate([
            {
                $lookup: {
                    "from": "invoices",
                    "let": {
                        inv_id: "$invoiceId"
                    },
                    "pipeline": [
                        {
                            $match: {
                                $expr: {
                                    $eq: [
                                        "$$inv_id",
                                        "$_id"
                                    ]
                                }
                            }
                        }
                    ],
                    as: "invoice"
                }
            },
            {
                $unwind: {
                    path: "$invoice"
                }
            },
            {
                $match: {
                    "invoice.type": "purchase"
                }
            },
            // {
            //     $unset: "invoice"
            // }
        ]);

        console.log('res::::', response);
        res.status(200).json({ res: response })
    } catch (error) {

    }
};


export {
    stockReport,
    saleReport
}