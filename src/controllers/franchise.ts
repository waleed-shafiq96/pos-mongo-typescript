import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import StoreUser from '../models/storeUser';
import store from '../models/store';
import franchise from '../models/franchise';
import roles from '../models/roles';



/*
*** Api for creating a franchise***
*/
const createFranchise = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(401).json({ msg: 'Not authorized' });
        }

        const newFranchise: any = await franchise.create({
            storeId: storeUser.storeId.id,
            name: req.body.franchiseName,
            phone: req.body.phone,
            address: req.body.address
        })

        if (!newFranchise) {
            return res.status(400).json({ msg: 'Franchise not created' })
        }

        res.status(200).json({ msg: 'franchise created', franchise: newFranchise })

    } catch (error) {
        res.status(500).json({ msg: 'Internal Server Error' })
    }

};

/*
*** Api for deleting a franchise***
*/
const deleteFranchise = async (req: Request, res: Response) => {

    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(401).json({ msg: 'Not authorized' });
        }

        const newFranchise: any = await franchise.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body.id,
            isDeleted: false
        }, {
            isDeleted: true
        });

        if (newFranchise.nModified === 0) {
            return res.status(400).json({ msg: 'Franchise Not deleted' });
        }

        res.status(200).json({ msg: 'franchise soft Deleted' });

    } catch (error) {
        console.log('error::::::', error);
        res.status(500).json({ msg: error.message })
    }

}


/*
*** Api for finding all franchises of a store***
*/
const findFranchises = async (req: Request, res: Response) => {
    try {
        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(401).json({ msg: 'Not authorized' });
        }

        const franchises: any = await franchise.find({
            storeId: storeUser.storeId.id,
            isDeleted: false
        }, '_id address name phone');

        if (!franchises[0]) {
            return res.status(404).json({ msg: 'No franchises found of your store' })
        }

        res.status(200).json({ msg: 'frachises found', franchises: franchises });

    } catch (error) {
        console.log('error::::::', error);
        res.status(500).json({ msg: 'Internal Server Error' })
    }

};

/*
*** Api for finding a franchise by id***
*/
const findFranchise = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(401).json({ msg: 'Not authorized' });
        }
        const foundFranchise: any = await franchise.findOne({
            storeId: storeUser.storeId.id,
            _id: req.body.id,
            isDeleted: false
        }, '_id isDeleted address name phone');

        if (!foundFranchise) {
            return res.status(404).json({ msg: 'No franchise found of this id' })
        }

        res.status(200).json({ msg: 'frachise found', franchise: foundFranchise });

    } catch (error) {
        res.status(500).json({ msg: error.message })
    }

};

/*
*** Api for editing a franchise by id***
*/
const editFranchise = async (req: Request, res: Response) => {
    try {

        const storeUser = req.body.user;

        if (storeUser.role.roleName !== 'store admin') {
            return res.status(401).json({ msg: 'Not authorized' });
        }

        const editedFranchise: any = await franchise.updateOne({
            storeId: storeUser.storeId.id,
            _id: req.body.id,
            isDeleted: false
        }, {
            name: req.body.name,
            phone: req.body.phone,
            address: req.body.address
        });

        if (editedFranchise.nModified == 0) {
            return res.status(404).json({ msg: 'No franchise edited of this id' })
        }

        res.status(200).json({ msg: 'frachise edited'});

    } catch (error) {
        res.status(500).json({ msg: error.message })
    }

};


export {
    createFranchise,
    deleteFranchise,
    findFranchises,
    findFranchise,
    editFranchise

}