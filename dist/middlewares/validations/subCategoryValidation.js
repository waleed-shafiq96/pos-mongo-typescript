"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const JOI = __importStar(require("@hapi/joi"));
const schema = JOI.object().keys({
    user: JOI.object().required(),
    subCategory: JOI.string().required(),
    categoryId: JOI.string().regex(/^[0-9a-fA-F]{24}$/).required(),
});
const createCategory = (req, res, next) => {
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).json(result.error);
    }
    else {
        next();
    }
};
exports.default = createCategory;
