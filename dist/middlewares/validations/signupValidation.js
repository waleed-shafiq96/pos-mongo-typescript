"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const JOI = __importStar(require("@hapi/joi"));
const schema = JOI.object().keys({
    email: JOI.string().email().required(),
    password: JOI.string().min(8).required(),
    storeName: JOI.string().required(),
    address: JOI.string().required()
});
const signupValidation = (req, res, next) => {
    const result = schema.validate(req.body);
    if (result.error) {
        return res.status(400).json(result.error);
    }
    else {
        next();
    }
};
exports.default = signupValidation;
