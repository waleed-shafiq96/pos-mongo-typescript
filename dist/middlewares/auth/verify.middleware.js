"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const JWT = __importStar(require("jsonwebtoken"));
const verify = (req, res, next) => {
    let authentication = req.headers.authorization;
    if (authentication) {
        let accessToken = authentication.split(' ')[1];
        JWT.verify(accessToken, `${process.env.jwtSecret}`, (err, decode) => {
            if (err) {
                return res.status(401).json({ message: 'Invalid token' });
            }
            req.body.user = decode;
            next();
        });
    }
    else {
        return res.status(401).json({ message: 'No token supplied' });
    }
};
exports.default = verify;
