"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const products = new mongoose_1.Schema({
    storeId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    productCode: { type: String },
    category: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'category'
    },
    costPrice: { type: Number },
    minimumRetailPrice: { type: Number },
    productTax: { type: Number },
    unit: { type: Number },
    status: { type: String },
    description: { type: String },
    minimumQuantity: { type: Number },
    maximumQuantity: { type: Number },
    openingStock: { type: Number },
    imageField: { type: String }
}, { timestamps: true });
// Export the model and return interface
exports.default = mongoose_1.default.model('products', products);
