"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const store = new mongoose_1.Schema({
    storeName: { type: String, required: true, unique: true },
    domain: { type: String, required: true },
    isDeleted: { type: Boolean, default: false },
    ownerName: { type: String },
    phone: { type: String },
    slogan: { type: String },
    storeAddress: { type: String },
    email: { type: String }
}, { timestamps: true });
// Export the model and return interface
exports.default = mongoose_1.default.model('store', store);
