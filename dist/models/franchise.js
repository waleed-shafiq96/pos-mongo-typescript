"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const franchise = new mongoose_1.Schema({
    storeId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    name: {
        type: String,
        required: true,
        default: 'main franchise'
    },
    address: {
        type: String
    },
    phone: { type: String },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
// Export the model and return interface
exports.default = mongoose_1.default.model('franchise', franchise);
