"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const superAdmin = new mongoose_1.Schema({
    name: { type: String, required: true, unique: true },
    userName: { type: String, required: true },
    password: { type: String, required: true },
    status: { type: Boolean },
    lasLogin: { type: Date }
});
// Export the model and return interface
exports.default = mongoose_1.default.model('superAdmin', superAdmin);
