"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const supplier = new mongoose_1.Schema({
    storeId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'store',
        required: true
    },
    franchiseId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'franchise',
        required: true
    },
    firstName: { type: String, required: true },
    lastName: { type: String },
    companyName: { type: String },
    address: { type: String },
    accountNo: { type: String },
    productType: { type: String },
    contactNo: { type: String },
    email: { type: String },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
// Export the model and return interface
exports.default = mongoose_1.default.model('supplier', supplier);
