"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const invoiceDetails = new mongoose_1.Schema({
    invoiceId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'invoice',
        required: true
    },
    franchiseProductId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'franchise-product',
        required: true
    },
    date: { type: Date },
    billType: { type: String },
    paymentMethod: { type: String },
    quantity: { type: Number },
    unitPrice: { type: Number },
    type: { type: String }
}, { timestamps: true });
// Export the model and return interface
exports.default = mongoose_1.default.model('invoice-details', invoiceDetails);
