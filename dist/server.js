"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const compression_1 = __importDefault(require("compression"));
const helmet_1 = __importDefault(require("helmet"));
const routes_1 = __importDefault(require("./routes"));
const cors_1 = __importDefault(require("cors"));
const bodyParser = __importStar(require("body-parser"));
const morgan_1 = __importDefault(require("morgan"));
const db_1 = __importDefault(require("./config/db"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const db = new db_1.default();
app_1.default.use(helmet_1.default()); // set well-known security-related HTTP headers
app_1.default.use(compression_1.default());
app_1.default.disable("x-powered-by");
app_1.default.use(cors_1.default());
app_1.default.use(morgan_1.default(':method :url :status :res[content-length] - :response-time ms'));
app_1.default.use(bodyParser.urlencoded({ extended: true }));
app_1.default.use(bodyParser.json({ limit: '50mb' }));
routes_1.default(app_1.default);
const server = app_1.default.listen(3000, () => console.log("Starting ExpressJS server on Port 3000"));
exports.default = server;
