"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const JWT = __importStar(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const jwt = (user) => {
    return JWT.sign({
        storeId: user.storeId.id,
        franchiseId: user.franchiseId.id,
        userId: user.id,
        email: user.email,
        roleId: user.role.id
    }, `${process.env.jwtSecret}`, { expiresIn: 300 * 300 });
};
exports.jwt = jwt;
const adminJwt = (user) => {
    return JWT.sign({
        storeId: user.storeId.id,
        userId: user.id,
        email: user.email,
        roleId: user.role.id
    }, `${process.env.jwtSecret}`, { expiresIn: 300 * 300 });
};
exports.adminJwt = adminJwt;
