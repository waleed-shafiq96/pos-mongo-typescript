"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_1 = require("../controllers/auth");
const validations_1 = require("../middlewares/validations");
const auth = (app) => {
    const prefix = '/auth';
    app.post(prefix + '/login', validations_1.loginValidation, auth_1.login);
};
exports.default = auth;
