"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const storeUser_1 = __importDefault(require("./storeUser"));
const auth_1 = __importDefault(require("./auth"));
const store_1 = __importDefault(require("./store"));
const franchise_1 = __importDefault(require("./franchise"));
const franchiseProduct_1 = __importDefault(require("./franchiseProduct"));
const products_1 = __importDefault(require("./products"));
const invoice_1 = __importDefault(require("./invoice"));
const supplier_1 = __importDefault(require("./supplier"));
const category_1 = __importDefault(require("./category"));
const report_1 = __importDefault(require("./report"));
const index = (app) => {
    app.get('/', (req, res) => {
        return res.json({ message: 'hello' });
    });
};
const allRoutes = (app) => {
    return {
        root: index(app),
        storeUser: storeUser_1.default(app),
        auth: auth_1.default(app),
        store: store_1.default(app),
        franchise: franchise_1.default(app),
        franchiseProduct: franchiseProduct_1.default(app),
        products: products_1.default(app),
        invoice: invoice_1.default(app),
        supplier: supplier_1.default(app),
        category: category_1.default(app),
        report: report_1.default(app)
    };
};
exports.default = allRoutes;
